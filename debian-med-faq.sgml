<!doctype linuxdoc system>
<article>
<title>Debian Med FAQ
<author>Andreas Tille <htmlurl url ="mailto:tille@debian.org" name="&lt;tille@debian.org&gt;">
<date>v0.2 19 February 2010
<abstract>
FAQ of the Debian Med project.
<toc>

<sect>General
<p>
Some general questions about the Debian Med project.
</p>

<sect1>Why Debian Med?
<p>
There are many free software projects for certain tasks in medical
care. Some of them do quite the same job others are very
different. The Debian operating system provides a rock solid base for
applications which require security and confidence.
</p><p>
So Debian Med uses this base to collect all those fine free software
for medical care in one place.  At first glance it is just for the
ease of installation and use of the Debian packaging system.  But the
projects also gain profit from the security system, the bug tracking
system and the wide user base which makes the software popular.
</p>

<sect1>What is Debian Med?
<p>
Debian Med is a <htmlurl url="http://blends.alioth.debian.org/blends"
name="Debian Pure Blend"> to support tasks of people in medical
care. The goal of Debian Med is a complete system for all tasks in
medical care which is build completely on free software.
</p>

<sect>Documentation
<p>
What about documentation in Debian Med?

<sect1>Which documents are included in Debian Med?
<p>
<htmlurl url="http://localhost/doc/medicine-howto/html" name="Medicine HOWTO">
</p>

<sect1>What about translation?
<p>
Translation of documentation and application interfaces are a main
goal of Debian Med.
</p>

<sect>Sources of information

<sect1>How to get an overview about all Debian Med meta packages?
<p>
Just have a look at the
<htmlurl
url="http://debian-med.alioth.debian.org/tasks" name="tasks page"> of
Debian Med.
</p>

<sect>Support
<p>
<sect1>How can I support Debian Med?
<p>
Search the Web for useful applications and ask for packaging.
</p><p>
<sect1>How to ask for packaging?
<p>
At first please read the documentation about
<htmlurl url="http://www.debian.org/devel/wnpp/"
         name="Work-Needing and Prospective Packages">.
</p><p>
File a Request For Packaging (RFP) bug report against the Work Needing
ans Prospective Packages (WNPP) package.
</p><p>
General rule to get something into Debian:
<itemize>
 <item>If there is some interest in a program which should be packaged
    this interest has to be announced first.
 <item>Two cases:
   <enum>
     <item>Request For Packaging (RFP)
        if you just want to ask other developers to care for a package
     <item> Intent To Package (ITP)
        if you want to build the package yourself.
        You can build the package even if you are not a Debian developer
        but you have to find a sponsor
        This is explained very detailed in the <htmlurl
        url="http://www.debian.org/devel/join/newmaint" 
        name="Debians New Maintainers Guide"> which you should read in
        any case before starting to build Debian packages.
   </enum>
 <item> One way:
    Use reportbug to announce your RFP / ITP

  <tscreen>
  <verb>                                                                            
      ~> su
      ~# apt-get install reportbug
      ~> exit
      ~> reportbug
          ...
         [file the bug against the package named 'wnpp']
  </verb>
  </tscreen>
 <item> make sure you include at least upstream URL and license of the
    program in the formular if you do not want to get flamed by
    developers from the debian-devel list
</itemize>

<sect1>Is it hard to build Debian packages?
<p>
Short answer: No.
</p><p>
Longer answer: The first thing to read is the <htmlurl
url="http://www.debian.org/devel/join/newmaint" name="Debians New
Maintainers Guide"> which contains important information how to
start. After reading this information a good point to start is
<tt>dh_make</> which is part of the <tt>debhelper</> tools.  Just have a look at
<tt>man dh_make</>.
</p>

<sect1>Which software can be distributed by Debian?
<p>
Make sure that it has a license which complies with the
<htmlurl url="http://www.debian.org/social_contract" name="Debian Free
Software Guidelines (DFSG)">.  If you are unsure about the license you
can ask at the mailing list <htmlurl
url="mailto:debian-legal@lists.debian.org" name="debian-legal">.
</p>
</article>
