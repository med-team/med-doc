#!/usr/bin/make -f
# Makefile for formating the Debian-Med FAQ

### HELP: I used a really dirty trick to get a real '&' into an url.
###       See below for the line
###             sed "s/\(?keywords=med-\)&amp;\(subword=1\)/\1\&\2/"
###       Anybody knows how to do this right in SGML??

faq=debian-med-faq

all: html 05_biomed_bio

html: $(faq).sgml
	linuxdoc --backend=html $(faq).sgml
#	ln -s $(faq).html index.html
	## I like output with the same timestamp as the source
	for html in `ls d*.html` ; do \
	   tmp=$${html}.$$ ; \
	   mv $${html} $${tmp} ; \
	   cat $${tmp} | \
	      sed "s?<TITLE>.*</TITLE>?&<link rel=\"stylesheet\" type=\"text/css\" href=\"med-doc.css\">?" | \
	      sed "s/\(?keywords=med-\)&amp;\(subword=1\)/\1\&\2/" | \
	   cat > $${html} ; \
	   rm -f $${tmp} ; \
	   touch -r $(faq).sgml $${html} ; \
	done

05_biomed_bio: 05_biomed_bio.stamp
05_biomed_bio.stamp:
	cd 05_biomed_bio; make pdf; make html; cd ..
	touch 05_biomed_bio.stamp

clean:
	rm -f $(faq)*.html
	cd 05_biomed_bio; make distclean; cd ..
	rm -f 05_biomed_bio.stamp
